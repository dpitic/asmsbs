;; Convert lowercase input from stdin to uppercase output to stdout.  This
;; program uses buffered I/O to read characters from stdin into a fixed size
;; buffer, convert character to uppercase if required, and output the result to
;; stdout.  Using a buffer enables the program to reduce the number of system
;; calls required to read and write, which should improve performance and
;; efficiency.  This program does not implement any error handling.

section .data                   ; Initialised data

section .bss                    ; Uninitialised data
  BUFLEN equ 1024               ; I/O buffer size (bytes)
buf: resb BUFLEN                ; I/O buffer

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point

read:                           ; Read stdin into buffer
  xor	rax, rax                  ; read() syscall identifier = 0
  xor	rdi, rdi                  ; stdin file descriptor = 0
  mov	rsi, buf                  ; pointer to buffer
  mov	rdx, BUFLEN               ; size of buffer (bytes)
  syscall                       ; rax = number of bytes read, -1 on error
  mov	rsi, rax                  ; rsi = number of bytes read
  cmp	rax, 0                    ; check if EOF
  je	exit                      ; EOF, end program

  ; Prepare for processing buffer
  mov	rcx, rsi                  ; rcx = buffer length counter = num bytes read
  mov	rbp, buf                  ; buffer start address
  dec	rbp                       ; adjust count to offset (sub optimal approach)

scan:                           ; Convert all lowercase char to uppercase
  cmp	byte [rbp+rcx], 0x61      ; check if char is 'a'
  jb	next                      ; if below 'a', not lowercase, skip processing
  cmp	byte [rbp+rcx], 0x7a      ; check if char is 'z'
  ja	next                      ; if above 'z', not lowercase, skip processing
  ; At this stage the char in the buffer is lowercase: process char
  sub	byte [rbp+rcx], 0x20      ; convert lowercase char to uppercase
next:                           ; process next character in buffer
  dec	rcx                       ; buffer length counter, when 0 finished
  jnz	scan                      ; continue processing buffer if counter != 0

write:                          ; Write lowercase char buffer to stdout
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 1                    ; stdout file descriptor = 1
  mov	rdx, rsi                  ; size of buffer read 
  mov	rsi, buf                  ; pointer to buffer to output
  syscall                       ; rax = number of bytes written, -1 on error
  jmp	read                      ; read more data into buffer

exit:                           ; Terminate program
  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall
