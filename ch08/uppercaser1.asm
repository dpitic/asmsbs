;; Convert lowercase input from stdin to uppercase output to stdout.  This 
;; program reads a single character at a time from stdin and converts it to
;; uppercase, if required, and outputs it to stdout.  Also, it does not have any
;; error handling code.

section .data                   ; Initialised data

section .bss                    ; Uninitialised data
buf: resb 1                     ; 1 byte read buffer

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point

read:                           ; Read character from stdin
  xor	rax, rax                  ; read() syscall identifier = 0
  xor	rdi, rdi                  ; stdin file descriptor = 0
  mov	rsi, buf                  ; read buffer
  mov	rdx, 1                    ; read size 1 byte
  syscall                       ; rax = number of bytes read, -1 on error

  cmp	rax, 0                    ; check if EOF
  je	exit                      ; EOF, exit program
  cmp	byte [buf], 0x61          ; check if lowercase 'a'
  jb	write                     ; if below 'a', not lowercase
  cmp	byte [buf], 0x7a          ; check if lowercase 'z'
  ja	write                     ; if above 'z', not lowercase
  ; At this stage, we have a lowercase character
  sub	byte [buf], 0x20          ; convert lowercase to uppercase

write:                          ; write (uppercase) char to stdout
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 1                    ; stdout file descriptor
  mov	rsi, buf                  ; pointer to buffer to write
  mov	rdx, 1                    ; write size 1 byte
  syscall                       ; rax = number of bytes written, -1 on error
  jmp	read                      ; get next character and repeat

exit:                           ; Exit program
  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall
