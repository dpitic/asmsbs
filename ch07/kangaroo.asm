;; This program demonstrates looping. There is no output. It is intended to to
;; be executed through a debugger.

section .data                   ; Initialised data
snippet: db "KANGAROO"

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point
  mov	rbx, snippet              ; initialise string pointer
  mov	rax, 8                    ; rax = counter (length of snippet string)
DoMore:
  add	byte [rbx], 32            ; transform to lower case
  inc	rbx                       ; increment string pointer
  dec	rax                       ; decrement string length counter
  jnz	DoMore

  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall

section .bss                    ; Uninitialised data
