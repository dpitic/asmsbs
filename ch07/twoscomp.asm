;; This program demonstrates what happens to a number when its value becomes
;; negative. It demonstrates two's complement. The program implements an endless
;; loop, so it is designed to be run through a debugger.

section .data                   ; Initialised data

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point
  mov	rax, 42
  neg	rax                       ; two's complement
  add	rax, 42                   ; 0
  
  mov	rax, 5                    ; observe the value of rax when it becomes < 0
DoMore:
  dec	rax
  jmp	DoMore                    ; this is an endless loop

  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall

section .bss                    ; Uninitialised data
