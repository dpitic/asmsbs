;; NASM sandbox template

section .data                   ; Initialised data

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point

  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall

section .bss                    ; Uninitialised data
