# Common make definitions

# Definitions required in all program diretories to assemble and link
# assembly programs using nasm.

AS = nasm
ASFLAGS = -felf64 -Wall -g -Fdwarf

# Common temp files to delete from each directory
TEMPFILES = core.* *.o temp.* *.out