;; Convert hexadecimal numbers to hexadecimal digits.  This program reads from
;; stdin 16 bytes at a time, and outputs those 16 bytes in a line as 16 hex
;; values separated by spaces.

section .data                   ; Initialised data
  ; Line string for output containing hex digits
hexstr: db " 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00",10
.len: equ $-hexstr
  ; Each digit in the hex lookup table occupies a position in the string whose
  ; offset from the start (0) of the string is the value it represents.
digits: db "0123456789ABCDEF"   ; hexadecimal digits lookup table

section .bss                    ; Uninitialised data
  BUFLEN equ 16                 ; read buffer size 16 bytes
buf: resb BUFLEN                ; read buffer

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point
  nop                           ; keeps gdb happy
read:                           ; Read a buffer full of text from stdin
  mov	rax, 0                    ; read() syscall identifier
  mov	rdi, 0                    ; stdin file descriptor
  mov	rsi, buf                  ; pointer to read buffer
  mov	rdx, BUFLEN               ; size of read buffer
  syscall                       ; rax = number of bytes read, -1 on error
  mov	rbp, rax                  ; save number of bytes read for later check
  cmp	rax, 0                    ; check for EOF
  je	done                      ; finish if EOF

  ; Initialise registers for buffer processing
  mov	rdi, hexstr               ; pointer to destination hex line string buffer
  xor	rcx, rcx                  ; initialise hex line string buffer index to 0

  ; Process read buffer and convert binary values to hex digits
scan:
  xor	rax, rax
  ; Calculate the offset into hexstr: RDX = RCX x 3
  mov	rdx, rcx                  ; copy line string buffer index
  shl	rdx, 1                    ; multiply pointer by 2 using SHL
  add	rdx, rcx                  ; complete multiplication by 3

  ; Get a character from the read buffer and put it in both RAX and RBX to split
  ; the byte into 2 nybbles. RAX = low (rightmost) digit, RBX = high (leftmost)
  ; digit.
  mov	al, byte [rsi+rcx]        ; put a byte from the read buffer into AL
  mov	rbx, rax                  ; duplicate the byte in BL for high nybble

  ; Look up low nybble character and insert it into the line string buffer
  and	al, 0x0f                  ; mask out all but the low nybble
  mov	al, byte [digits+rax]     ; look up the char equivalent of nybble
  mov	byte [hexstr+rdx+2], al   ; write LSB char digit to line string buffer

  ; Look up high nybble character and insert it into the line string buffer
  shr	bl, 4                     ; shift high 4 bits of char into low 4 bits
  mov	bl, byte [digits+rbx]     ; look up char equivalent of nybble
  mov	byte [hexstr+rdx+1], bl   ; write MSB char digit to line string buffer

  ; Bump the read buffer pointer to next character and see if finished
  inc	rcx                       ; increment line string buffer index
  cmp	rcx, rbp                  ; cmp index with number of chars read in buffer
  jna	scan                      ; continue processing if RCX <= read buffer len

  ; Write a line of hexadecimal values to stdout
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 1                    ; stdout file descriptor
  mov	rsi, hexstr               ; pointer to string buffer to output
  mov	rdx, hexstr.len           ; length of line string
  syscall                       ; rax = number of bytes output, -1 on error
  jmp	read                      ; read more input

done:                           ; exit program
  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall
