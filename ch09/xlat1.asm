;; This program demonstrates the use of the XLAT instruction to translate stdin
;; stream to uppercase output on stdout.

section .data                   ; Initialised data
statmsg: db "Processing...", 10
.len: equ $-statmsg
donemsg: db "...done!", 10
.len: equ $-donemsg

;; This translation table is used to translate all lowercase characters to
;; uppercase. It also translates all non-printable characters to spaces,
;; except for LF and HT.
upcase:
  db 20h,20h,20h,20h,20h,20h,20h,20h,20h,09h,0Ah,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,21h,22h,23h,24h,25h,26h,27h,28h,29h,2Ah,2Bh,2Ch,2Dh,2Eh,2Fh
	db 30h,31h,32h,33h,34h,35h,36h,37h,38h,39h,3Ah,3Bh,3Ch,3Dh,3Eh,3Fh
	db 40h,41h,42h,43h,44h,45h,46h,47h,48h,49h,4Ah,4Bh,4Ch,4Dh,4Eh,4Fh
	db 50h,51h,52h,53h,54h,55h,56h,57h,58h,59h,5Ah,5Bh,5Ch,5Dh,5Eh,5Fh
	db 60h,41h,42h,43h,44h,45h,46h,47h,48h,49h,4Ah,4Bh,4Ch,4Dh,4Eh,4Fh
	db 50h,51h,52h,53h,54h,55h,56h,57h,58h,59h,5Ah,7Bh,7Ch,7Dh,7Eh,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
	db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h

section .bss                    ; Uninitialised data
  READLEN equ 1024              ; read length buffer
rbuf: resb READLEN              ; read buffer

section .text                   ; Machine instructions
global _start                   ; export program entry point

_start:                         ; program entry point
  nop                           ; keep GDB happy

  ; Display the start message on stderr
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 2                    ; stderr file descriptor
  mov	rsi, statmsg              ; pointer to string buffer to output
  mov	rdx, statmsg.len          ; string buffer length
  syscall                       ; rax = number of bytes written, -1 on error

read:                           ; Read buffer full of text from stdin
  mov	rax, 0                    ; read() syscall identifier
  mov	rdi, 0                    ; stdin file descriptor
  mov	rsi, rbuf                 ; pointer to read buffer
  mov	rdx, READLEN              ; read buffer length
  syscall                       ; rax = number of bytes read, -1 on error
  mov	rbp, rax                  ; copy number of bytes read
  cmp	rax, 0                    ; check if EOF
  je	done                      ; done if EOF

  ; Set up registers for translate
  mov	rbx, upcase               ; base address of translation table
  mov	rdx, rbuf                 ; pointer of read buffer
  mov	rcx, rbp                  ; number of bytes read

translate:                      ; Translate the data in the read buffer
  mov	al, byte [rdx+rcx]        ; load character into AL for translation
  xlatb                         ; AL = translated character
  mov	byte [rdx+rcx], al        ; replace character in buffer with translation
  dec	rcx                       ; move buffer index to next character
  jnz	translate                 ; continue translating

write:                          ; Write translated buffer to stdout
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 1                    ; stdout file descriptor
  mov	rsi, rbuf                 ; pointer to buffer to output
  mov	rdx, rbp                  ; number of bytes in read buffer translated
  syscall                       ; rax = number of bytes written, -1 on error
  jmp	read                      ; continue reading from stdin

done:                           ; Display finish message to stderr
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 2                    ; stderr file descriptor
  mov	rsi, donemsg              ; pointer to output string buffer
  mov	rdx, donemsg.len          ; length of output string buffer
  syscall                       ; rax = number of bytes written, -1 on error

  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall
