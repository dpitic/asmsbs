;; A simple hex dump utility demonstrating the use of assembly language
;; procedures.

section .data                   ; Initialised data

  ; This is two parts of a single useful data structure, implementing the text
  ; line of a hex dump utility. The first part displays 16 bytes in hex
  ; separated by spaces. Immediately following is a 16 character line delimited
  ; by vertical bar characters. Because they are adjacent, the two parts can be
  ; referenced separately or as a single contiguous unit. Remember that if
  ; dumplin is to be used separately, you must append an EOL before sending it
  ; the console.
dumplin: db " 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
  dumplen equ $-dumplin
asclin: db "|................|",10
  asclen equ $-asclin
  fullen equ $-dumplin

  ; The hexdigits table is used to convert numeric values to their hex
  ; equivalent. Index by nybble without a scale: [hexdigits+rax]
hexdigits: db "0123456789ABCDEF"

  ; This table is used for ASCII character translation, into the ASCII portion
  ; of the hex dump line, via XLAT or ordinary memory lookup. All printable
  ; characters "play through" as themselves. The high 128 characters are
  ; translated to ASCII period (0x2E). The non-printable characters in the low
  ; 128 are also translated to ASCII period, as is char 127.
dotxlat:
  db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 20h,21h,22h,23h,24h,25h,26h,27h,28h,29h,2Ah,2Bh,2Ch,2Dh,2Eh,2Fh
	db 30h,31h,32h,33h,34h,35h,36h,37h,38h,39h,3Ah,3Bh,3Ch,3Dh,3Eh,3Fh
	db 40h,41h,42h,43h,44h,45h,46h,47h,48h,49h,4Ah,4Bh,4Ch,4Dh,4Eh,4Fh
	db 50h,51h,52h,53h,54h,55h,56h,57h,58h,59h,5Ah,5Bh,5Ch,5Dh,5Eh,5Fh
	db 60h,61h,62h,63h,64h,65h,66h,67h,68h,69h,6Ah,6Bh,6Ch,6Dh,6Eh,6Fh
	db 70h,71h,72h,73h,74h,75h,76h,77h,78h,79h,7Ah,7Bh,7Ch,7Dh,7Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh
	db 2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh,2Eh

section .bss                    ; Uninitialised data
  buflen equ 10
  buf resb buflen

section .text                   ; Machine instructions

;;
;; @brief      Clear a hex dump line string to 16, 0 values
;;
;; @details    The hex dump line string is cleared to binary 0 by calling
;;             dumpchar 16 times, passing 0 each time.
;;
clearline:
  ; Save caller's GP registers
  push	rax
  push	rcx
  push	rdx
  push	rdi
  push	rsi
  mov	rdx, 15                   ; insert 16 times, counting from 0
.poke:
  mov	rax, 0                    ; '0' value to insert in hex dump string
  call	dumpchar                ; insert '0' into the hex dump string
  sub	rdx, 1                    ; DEC doesn't affect CF
  jae	.poke                     ; continue loop if RDX >= 0
  pop	rsi                       ; restore caller's registers
  pop	rdi
  pop	rdx
  pop	rcx
  pop	rax
  ret

;;
;; @brief      Insert a value into the hex dump line string.
;;
;; @details    The value passed in RAX will be put in both the hex dump portion
;;             and in the ASCII portion, at the position passed in RDX,
;;             represented by a space where it is not a printable character.
;;
;; @param      RAX 8-bit value to be inserted.
;;             RDX position (0-15) in the hex dump line string to insert value.
;;
dumpchar:
  push	rbx                     ; save caller's RBX
  push	rdi                     ; save caller's RDI
  ; Insert the input char into the ASCII portion of the dump line
  mov	bl, byte [dotxlat+rax]    ; translate non printable chars to '.'
  mov	byte [asclin+rdx+1], bl   ; write to ASCII portion
  ; Insert the hex equivalent of the input char in the hex portion of the hex
  ; dump line.
  mov	rbx, rax                  ; save a second copy of the input char
  lea	rdi, [rdx*2+rdx]          ; calculate offset into line string (RDX x 3)
  ; Look up low nybble character and insert it into the string
  and	rax, 0x0000000f           ; mask out all but the low nybble
  mov	al, byte [hexdigits+rax]  ; look up the char equivalent to line string
  mov	byte [dumplin+rdi+2], al  ; write the char equivalent to line string
  ; Look up high nybble character and insert it into the string
  and	rbx, 0x000000f0           ; mask out all but the second-lowest nybble
  shr	rbx, 4                    ; shift high 4 buts of byte into low 4 bits
  mov	bl, byte [hexdigits+rbx]  ; look up char equivalent of nybble
  mov	byte [dumplin+rdi+1], bl  ; write the char equivalent to line string
  pop	rdi                       ; finished, restore caller's RDI
  pop	rbx                       ; restore caller's RBX
  ret

;;
;; @brief      Display dumplin to stdout.
;;
;; @details    The hex dump line string dumplin is displayed to stdout using
;;             using write() syscall.  All GP registers are preserved.
;;
printline:
  ; Save caller's GP registers
  push	rax
  push	rcx
  push	rdx
  push	rdi
  push	rsi
  mov	rax, 1                    ; write() syscall identifier
  mov	rdi, 1                    ; stdout file descriptor
  mov	rsi, dumplin              ; pointer to string buffer
  mov	rdx, dumplen              ; size of string buffer
  syscall                       ; RAX = number of bytes written, -1 on error
  pop	rsi                       ; restore caller's registers
  pop	rdi
  pop	rdx
  pop	rcx
  pop	rax
  ret

;;
;; @brief      Fills a buffer with data from stdin via read() syscall.
;;
;; @details    Loads a buffer full of data (buflen bytes) from stdin using
;;             read() syscall and places it in buf.  Buffer offset counter RSI
;;             is zeroed, because we're starting in on a new buffer full of
;;             data.  Caller must test value in RBP: if RBP contains zero on
;;             return, we hit EOF on stdin.  Less than 0 in RBP on return
;;             indicates some kind of error.
;;
;; @return     RBP = number of bytes read, -1 on error.
;;
loadbuf:
  push	rax                     ; save caller's registers
  push	rbx
  push	rdx
  mov	rax, 0                    ; read() syscall identifier
  mov	rdi, 0                    ; stdin file descriptor
  mov	rsi, buf                  ; pointer to read buffer
  mov	rdx, buflen               ; number of bytes to read
  syscall                       ; RAX = number of bytes read, -1 on error
  mov	rbp, rax                  ; return number of bytes read in RBP
  xor	rsi, rsi                  ; clear buffer pointer
  pop rdx                       ; restore caller's registers
  pop rbx
  pop rax
  ret

;;------------------------------------------------------------------------------
;; MAIN PROGRAM
;;------------------------------------------------------------------------------
global _start                   ; export program entry point
_start:                         ; program entry point
  nop                           ; nop for GDB
  ; Loop initialisation
  xor	rsi, rsi                  ; clear total byte counter
  call	loadbuf                 ; read into buffer, RBP = number of chars read
  cmp	rbp, 0                    ; check for EOF or error
  jbe	exit
  xor	rcx, rcx                  ; zero buffer pointer
  ; Iterate through buffer and convert binary byte values to hex digits
scan:
  xor	rax, rax                  ; clear RAX
  mov	al, byte[buf+rcx]         ; get byte from buffer into AL
  mov	rdx, rsi                  ; copy total counter into RDX
  and	rdx, 0x0f                 ; mask out lowest 4 bits of char counter
  call	dumpchar                ; insert value into hex dump line string
  ; Increment buffer pointer to next character and see if reached end of buffer
  inc	rsi                       ; increment total chars processed counter
  inc	rcx                       ; increment buffer pointer
  cmp	rcx, rbp                  ; compare buffer pointer with # chars read
  jb	.modtest                  ; if processed all chars in buffer
  call	loadbuf                 ; get another buffer load of chars
  cmp	rbp, 0                    ; check if EOF or error
  jbe	done                      ; EOF done
  ; Check if reached end of block of 16 and need to display a line
.modtest:
  test	rsi, 0x0f               ; test 4 lowest bits in counter for 0
  jnz	scan                      ; if counter is not modulo 16, loop back
  call	printline               ; print the line
  call	clearline               ; clear hex dump line to 0's
  jmp	scan                      ; continue scanning the buffer

done:                           ; all done
  call	printline               ; print the remaining line

exit:                           ; exit program
  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall
