;; Demonstration of using system calls to display text string to stdout.

section .data                   ; Initialised data
EatMsg: db "Eat at Joe's!", 10  ; output string
.len: equ $ - EatMsg            ; output string length

section .text                   ; Machine instructions
global _start                   ; Export entry point

_start:                         ; Entry point
  mov	rax, 1                    ; write() syscall identifier
  mov rdi, 1                    ; stdout file descriptor
  mov rsi, EatMsg               ; pointer to message string
  mov rdx, EatMsg.len           ; length of string
  syscall                       ; rax = number of bytes written

  mov rax, 60                   ; exit() syscall identifier
  xor rdi, rdi                  ; exit status 0
  syscall
